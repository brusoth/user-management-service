package com.beyond.usermanagementservice.repository.persistence

import com.beyond.usermanagementservice.repository.entity.VerificationToken
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TokenRepository : JpaRepository<VerificationToken, String> {
    fun findByToken(token: String): VerificationToken?
}