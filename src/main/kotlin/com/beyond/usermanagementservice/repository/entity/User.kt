package com.beyond.usermanagementservice.repository.entity

import javax.persistence.*

@Entity(name = "user_details")
data class User(
        @Id
        @Column(unique = true, nullable = false)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int,

        var firstName: String,

        var lastName: String,

        @Column(length = 60)
        var password: String,
        @Column
        var enabled: Boolean = false,

        val isUsing2FA: Boolean = false,

        val secret: String = "",

        var email: String
)