package com.beyond.usermanagementservice.repository.entity

import java.util.*
import javax.persistence.*


@Entity(name = "verification_token")
data class VerificationToken(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        var token: String,
        @OneToOne(targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(nullable = false, name = "user_id")
        val user: User,
        var expiryDate: Date) {
}