package com.beyond.usermanagementservice.exception

class ExistingUserException(message: String) : Throwable(message)