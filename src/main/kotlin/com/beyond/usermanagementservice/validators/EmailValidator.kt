package com.beyond.usermanagementservice.validators

import com.beyond.usermanagementservice.annotations.ValidEmail
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class EmailValidator : ConstraintValidator<ValidEmail, String> {
    private val EMAIL_PATTERN: String = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$"
    private lateinit var pattern: Pattern
    private lateinit var matcher: Matcher

    override fun isValid(email: String?, p1: ConstraintValidatorContext?): Boolean {
        if (email == null)
            return false
        return validateEmail(email)
    }

    private fun validateEmail(email: String?): Boolean {
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }
}
