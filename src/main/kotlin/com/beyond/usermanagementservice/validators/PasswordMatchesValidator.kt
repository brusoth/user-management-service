package com.beyond.usermanagementservice.validators

import com.beyond.usermanagementservice.annotations.PasswordMatches
import com.beyond.usermanagementservice.dto.UserDto

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class PasswordMatchesValidator : ConstraintValidator<PasswordMatches, Any> {

    override fun initialize(constraintAnnotation: PasswordMatches?) {
        // method left blank to avoid initialize implementation
    }

    override fun isValid(obj: Any, constraintValidatorContext: ConstraintValidatorContext): Boolean {
        val userDto = obj as UserDto
        return userDto.password != null && userDto.password == userDto.confirmPassword
    }
}