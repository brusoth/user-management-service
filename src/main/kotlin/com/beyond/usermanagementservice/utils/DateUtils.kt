package com.beyond.usermanagementservice.utils

import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.util.*

@Component
class DateUtils {
    companion object {
        fun calculateExpiryDate(expiryTimeInMinutes: Int): Date {
            val cal = Calendar.getInstance()
            cal.time = Timestamp(cal.time.time)
            cal.add(Calendar.MINUTE, expiryTimeInMinutes)
            return Date(cal.time.time)
        }
    }
}