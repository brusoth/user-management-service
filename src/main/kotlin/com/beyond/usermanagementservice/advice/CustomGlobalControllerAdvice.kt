package com.beyond.usermanagementservice.advice

import com.beyond.usermanagementservice.controller.UserController
import com.beyond.usermanagementservice.exception.ExistingUserException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*
import java.util.stream.Collectors


@ControllerAdvice(basePackageClasses = [UserController::class])
class CustomGlobalControllerAdvice : ResponseEntityExceptionHandler() {
    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException,
                                              headers: HttpHeaders,
                                              status: HttpStatus, request: WebRequest): ResponseEntity<Any> {

        val body = LinkedHashMap<String, Any>()
        body["timestamp"] = Date()
        body["status"] = status.value()

        // get all errors
        val errors: List<String?> = ex.bindingResult
                .fieldErrors.union(ex.bindingResult.allErrors)
                .stream()
                .map { x -> x.defaultMessage }
                .collect(Collectors.toList())

        body["errors"] = errors

        return ResponseEntity(body, headers, status)

    }

    @ExceptionHandler(value = [(ExistingUserException::class)])
    fun handleUserAlreadyExistException(ex: ExistingUserException): ResponseEntity<Any> {
        val body = LinkedHashMap<String, Any>()
        body["timestamp"] = Date()
        body["status"] = 400
        val errors: List<String?> = listOf(ex.message)
        body["errors"] = errors

        return ResponseEntity(body, null, HttpStatus.BAD_REQUEST)
    }
}