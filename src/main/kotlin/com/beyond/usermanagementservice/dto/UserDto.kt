package com.beyond.usermanagementservice.dto

import com.beyond.usermanagementservice.annotations.PasswordMatches
import com.beyond.usermanagementservice.annotations.ValidEmail
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@PasswordMatches(message = "Password and Confirm Password not matching")
class UserDto {
    @NotBlank(message = "{firstname.empty}")
    val firstName: String? = null
    @NotBlank(message = "{lastname.empty}")
    val lastName: String? = null
    @NotBlank(message = "{password.empty}")
    val password: String? = null
    @NotBlank(message = "{password.confirm.empty}")
    val confirmPassword: String? = null
    @NotEmpty(message = "{email.empty}")
    @ValidEmail(message = "{email.invalid}")
    val email: String? = null
}
