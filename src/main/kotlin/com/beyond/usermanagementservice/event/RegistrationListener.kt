package com.beyond.usermanagementservice.event

import com.beyond.usermanagementservice.service.UserService
import org.springframework.context.ApplicationListener
import org.springframework.context.MessageSource
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Component
import java.util.*

@Component
class RegistrationListener(private val userService: UserService,
                           private val javaMailSender: JavaMailSender,
                           private val messageSource: MessageSource) : ApplicationListener<OnRegistrationCompleteEvent> {

    override fun onApplicationEvent(event: OnRegistrationCompleteEvent) {
        this.confirmRegistration(event)
    }

    private fun confirmRegistration(event: OnRegistrationCompleteEvent) {
        val user = event.user
        val token = UUID.randomUUID().toString()
        userService.createVerificationToken(user, token)

        val recipientEmail = user.email
        val subject = "Complete Registration"
        val confirmUrl = event.appUrl + "/registration/confirm?token=" + token

        val email = SimpleMailMessage()
        email.setTo(recipientEmail)
        email.setSubject(subject)
        email.setText("activate : $confirmUrl")
        javaMailSender.send(email)
    }
}