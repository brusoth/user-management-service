package com.beyond.usermanagementservice.event

import com.beyond.usermanagementservice.repository.entity.User
import org.springframework.context.ApplicationEvent
import java.util.*

data class OnRegistrationCompleteEvent(
        val appUrl: String,
        val locale: Locale,
        val user: User) : ApplicationEvent(user)