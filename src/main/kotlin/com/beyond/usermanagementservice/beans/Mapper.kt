package com.beyond.usermanagementservice.beans

import org.modelmapper.ModelMapper
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component


@Component
class Mapper {
    @Bean
    fun modelMapper(): ModelMapper {
        return ModelMapper()
    }
}
