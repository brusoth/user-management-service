package com.beyond.usermanagementservice.service

import com.beyond.usermanagementservice.exception.ExistingUserException
import com.beyond.usermanagementservice.repository.entity.User
import com.beyond.usermanagementservice.repository.entity.VerificationToken
import com.beyond.usermanagementservice.repository.persistence.TokenRepository
import com.beyond.usermanagementservice.repository.persistence.UserRepository
import com.beyond.usermanagementservice.utils.DateUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
open class UserService(private val userRepository: UserRepository,
                       private val tokenRepository: TokenRepository) {
    val logger = LoggerFactory.getLogger(UserService::class.java.name)
    @Throws(ExistingUserException::class)
    @Transactional
    open fun register(user: User): User {
        if (userExist(user))
            throw ExistingUserException("User is already registered with email id : ${user.email}")
        return userRepository.save(user)
    }

    private fun userExist(user: User): Boolean {
        val resultUser: User? = userRepository.findByEmail(user.email)
        return resultUser != null
    }

    open fun createVerificationToken(user: User, token: String) {
        val verificationToken = VerificationToken(0,
                token = token,
                user = user,
                expiryDate = DateUtils.calculateExpiryDate(60 * 24))
        tokenRepository.save(verificationToken)
    }

    open fun getVerificationToken(token: String): VerificationToken? {
        return tokenRepository.findByToken(token)
    }

    open fun saveUser(user: User) {
        logger.info("saving user ${user.firstName}")
        userRepository.save(user)
    }

    open fun generateNewVerificationToken(token: String): VerificationToken? {
        val existingToken = tokenRepository.findByToken(token)
        if (existingToken == null) {
            logger.info("existing token for token id : {} not found", token)
            return null
        }
        val newTokenId = UUID.randomUUID().toString()
        existingToken.token = newTokenId
        existingToken.expiryDate = DateUtils.calculateExpiryDate(60 * 24)
        return tokenRepository.save(existingToken)
    }
}
