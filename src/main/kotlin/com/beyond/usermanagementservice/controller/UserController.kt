package com.beyond.usermanagementservice.controller

import com.beyond.usermanagementservice.dto.UserDto
import com.beyond.usermanagementservice.event.OnRegistrationCompleteEvent
import com.beyond.usermanagementservice.repository.entity.User
import com.beyond.usermanagementservice.service.UserService
import org.modelmapper.ModelMapper
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class UserController(private val userService: UserService,
                     private val modelMapper: ModelMapper,
                     private val passwordEncoder: PasswordEncoder,
                     private val eventPublisher: ApplicationEventPublisher) {
    private val logger = LoggerFactory.getLogger(UserController::class.java.name)
    @PostMapping("/user")
    fun submitUser(@Valid @RequestBody userDto: UserDto,
                   request: HttpServletRequest): ResponseEntity<Any> {
        val user = convertToEntity(userDto)
        val registeredUser: User = userService.register(user)
        logger.info("user created with email : {}", registeredUser.email)
        eventPublisher.publishEvent(OnRegistrationCompleteEvent("http://${request.serverName}:${request.serverPort}${request.contextPath}",
                request.locale,
                registeredUser))
        return ResponseEntity(registeredUser, HttpStatus.OK)
    }

    @GetMapping("/user/resendRegistrationToken")
    fun resendRegistrationToken(@RequestParam("token") existingToken: String,
                                request: HttpServletRequest): ResponseEntity<String> {
        val verificationToken = userService.generateNewVerificationToken(token = existingToken)
        if (verificationToken == null) {
            logger.info("verification token not found.")
            return ResponseEntity("verification token not found", HttpStatus.NOT_FOUND)
        }
        val user = verificationToken.user
        eventPublisher.publishEvent(OnRegistrationCompleteEvent("http://${request.serverName}:${request.serverPort}${request.contextPath}",
                request.locale,
                user))
        return ResponseEntity("activation email sent", HttpStatus.OK)
    }

    @GetMapping("/user/confirm")
    fun confirmUser(@RequestParam token: String): ResponseEntity<String> {
        if (token.isBlank()) {
            logger.info("empty token received. validation failed")
            return ResponseEntity("activation failed", HttpStatus.BAD_REQUEST)
        }
        val verificationToken = userService.getVerificationToken(token)
        if (verificationToken == null) {
            logger.info("user not found for verification token")
            return ResponseEntity("activation failed", HttpStatus.BAD_REQUEST)
        }
        logger.info("user found with email : ${verificationToken.user.email}")
        if (verificationToken.expiryDate.time - Calendar.getInstance().time.time < 0) {
            logger.info("verification token expired")
            return ResponseEntity("activation failed. expired token", HttpStatus.BAD_REQUEST)
        }
        val user = verificationToken.user
        user.enabled = true
        userService.saveUser(user)
        return ResponseEntity("activated", HttpStatus.OK)
    }

    private fun convertToEntity(userDto: UserDto): User {
        val user = modelMapper.map(userDto, User::class.java)
        user.password = passwordEncoder.encode(user.password)
        return user
    }
}