CREATE TABLE user_details
(
    id         bigint(20)   NOT NULL AUTO_INCREMENT,
    first_name varchar(100) NOT NULL,
    last_name  varchar(100) NOT NULL,
    email      varchar(100) NOT NULL,
    password   varchar(60)  NOT NULL,
    enabled    boolean DEFAULT false,
    is_using2fa boolean DEFAULT false,
    secret     varchar(255),
    PRIMARY KEY (id),
    UNIQUE KEY (email)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;