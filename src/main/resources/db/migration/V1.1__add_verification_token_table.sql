CREATE TABLE verification_token
(
    id bigint(20) NOT NULL AUTO_INCREMENT,
    token text,
    user_id bigint(20),
    expiry_date DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users.user_details(id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;